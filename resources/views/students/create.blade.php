 @extends('layouts.app')
@section('content')
<div class="container my-5">
    <div class="row px-4">
        <form action="{{ route('students.store') }}" method="POST">
            @csrf
            <div class="row g-3 mb-3">
                <div class="col-4">
                    <label for="clientname" class="form-label">Name</label>
                    <input type="text" class="form-control" id="name" name="name" aria-describedby="name">
                </div>
                <div class="col-4">
                    <label for="phone" class="form-label">Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone" aria-describedby="phone">
                </div>
                <div class="col-4">
                    <label for="address" class="form-label">Address</label>
                    <input type="text" class="form-control" id="address" name="address" aria-describedby="address">
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection